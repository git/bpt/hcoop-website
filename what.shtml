<!--#include file="header.html" -->
<div id="main">
<h3>What We Offer</h3>

<p>Our service is built upon Debian GNU/Linux, Kerberos,
OpenAFS, and our custom services manager <a
href="http://wiki.hcoop.net/DomTool">Domtool</a>. Using
Domtool, you can configure the behavior of your domains
and sites in shared daemons without needing to request
anything from a superuser. Since we provide a generic
Debian GNU/Linux shell, you can write and run your own
daemons in whatever programming language you fancy.</p>

<p>We support Free Software, releasing all of our custom software <a
href="http://wiki.hcoop.net/Code">under Free licenses</a>, and using
only Free Software for our infrastructure.</p>

<p>Our <a
href="http://wiki.hcoop.net/Hardware">servers</a> are
colocated at <a href="http://peer1.net">Peer1</a> in New
York City. They have excellent connectivity, several
layers of power redundancy, and helpful on-site
staff.</p>

<table>
  <colgroup>
    <col style="width: 25%" />
      <col style="width: 70%" />
  </colgroup>

  <tr><td><strong>Storage</strong></td><td>Around ten gigabytes of
      storage. You can request more disk space as needed; we have
      quotas in place to prevent over-subscription of storage and
      strive to grant any reasonable requests so long as we have the
      resources for it. As we add more storage, all members
      benefit.</td></tr>

  <tr><td><strong>Bandwidth</strong></td><td>We currently have a 5Mbps
  connection (<a
  href="http://en.wikipedia.org/wiki/Burstable_billing">95th
  percentile</a> billing) burstable to 100Mbps. If your sites see less
  than fifty or so gigabytes of traffic per month, we can feasibly
  host you.</td></tr>

  <tr><td><strong>OpenAFS</strong></td><td>A
      publicly accessible <a
        href="http://openafs.org">OpenAFS</a> cell
      (<tt>/afs/hcoop.net/</tt> the whole world
      round). This lets you access your HCoop storage as
      if it were local disk on every major operating
      system.</td></tr>

  <tr><td><strong>Web Hosting</strong></td><td>General web hosting
        with Apache, featuring the usual languages (Perl, PHP, Python)
        and databases (PostgreSQL and MySQL). Unlike many providers,
        we'll install most anything available in Debian so you can run
        that Haskell web application with ease.</td></tr>

  <tr><td><strong>Email</strong></td><td>Mail hosting for unlimited
      domains (including <a
      href="https://webmail.hcoop.net">webmail</a> through Roundcube,
      IMAP, and POP3 access), with the ability to create virtual
      mailboxes for friends and family.</td></tr>

  <tr><td><strong>Mailing Lists</strong></td><td>Mailing lists through
  GNU Mailman. Members <a
  href="https://lists.hcoop.net/listinfo/">host lists</a> for pretty
  much anything: volunteer organizations, Linux User Groups,
  programming language development, even coordinating homebrewing
  sessions.</td></tr>

  <tr><td><strong>Shell</strong></td><td>A shell account on a machine running Debian's
      latest stable release. We don't mind folks idling
      on Freenode, and you're free to do things like
      building software.</td></tr>

  <tr><td><strong>Etc.</strong></td><td>Volunteers host shared
      services for the benefit of all members. These currently include
      a <a href="http://git.hcoop.net">git server</a>, managed <a
      href="http://moinmo.in/">MoinMoin</a> install, and Jabber
      (<tt>YOU@hcoop.net</tt>). We hope to offer GNU Mediagoblin,
      Diaspora*, Gitorious, darcsweb, and other services in the future
      (or: insert your idea here).</td></tr>
</table>
</div>
<!--#include file="footer.html" -->
